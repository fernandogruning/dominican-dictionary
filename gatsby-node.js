const path = require('path')

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulEntry {
          edges {
            node {
              slug
              entryCategory {
                slug
              }
            }
          }
        }
        allContentfulEntryCategory(filter: { entry: { elemMatch: { id: { ne: null } } } }) {
          edges {
            node {
              slug
            }
          }
        }
      }
    `).then(result => {
        result.data.allContentfulEntry.edges.forEach(({ node }) => {
          createPage({
            path: `${node.entryCategory.slug}/${node.slug}`,
            component: path.resolve(`./src/templates/contentful-entry.js`),
            context: {
              slug: node.slug,
              categorySlug: node.entryCategory.slug
            }
          })
        })

        result.data.allContentfulEntryCategory.edges.forEach(({ node }) => {
          createPage({
            path: node.slug,
            component: path.resolve(`./src/templates/contentful-category.js`),
            context: {
              slug: node.slug
            }
          })
        })

        resolve()
      })
  })
}
