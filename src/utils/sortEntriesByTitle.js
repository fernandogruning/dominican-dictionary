const sortEntriesByTitle = (prevEntry, nextEntry) => {
  const prevEntryTitle = prevEntry.title.toUpperCase()
  const nextEntryTitle = nextEntry.title.toUpperCase()

  if (prevEntryTitle > nextEntryTitle) return 1
  if (prevEntryTitle < nextEntryTitle) return -1
  return 0
}

export default sortEntriesByTitle
