import { rgba, darken } from "polished";

export default {
  primaryColor: `salmon`,
  bodyColor: rgba("black", 0.7),
  headingsColor: rgba("black", 0.8),
  linkColor: `salmon`,
  linkColorHover: darken(0.08, `salmon`)
}
