export default (categorySlug, entrySlug) => {
  return `${categorySlug}/${entrySlug}`
}
