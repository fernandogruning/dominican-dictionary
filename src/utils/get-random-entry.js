import getSlug from "./get-slug";

export default (allContentfulEntries) => {
  const randomEntry = allContentfulEntries[Math.floor(Math.random() * allContentfulEntries.length)].node
  const randomEntryId = randomEntry.id
  const randomEntryTitle = randomEntry.title
  const randomEntryDescription = randomEntry.description.childMarkdownRemark.html
  const randomEntrySlug = getSlug(randomEntry.entryCategory.slug, randomEntry.slug)

  return {
    id: randomEntryId,
    title: randomEntryTitle,
    description: randomEntryDescription,
    slug: randomEntrySlug
  }
}
