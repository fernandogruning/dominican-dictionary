import Typography from 'typography'
import globalTheme from './global-theme';

const typography = new Typography({
  baseFontSize: `16px`,
  scaleRatio: 3,
  googleFonts: [
    {
      name: `Noto Serif`,
      styles: [`400`, `400i`, `700`, `700i`]
    },
    {
      name: `Work Sans`,
      styles: [`400`, `500`, `600`]
    }
  ],
  headerFontFamily: ['Noto Serif', 'serif'],
  headerWeight: 700,
  headerColor: globalTheme.headingsColor,
  bodyFontFamily: ['Noto Serif', 'serif'],
  bodyWeight: 400,
  bodyColor: globalTheme.bodyColor,
  overrideStyles: ({ adjustFontSizeTo, scale, rhythm }, options) => ({
    a: {
      color: globalTheme.linkColor,
      textDecoration: `none`,
      transition: `all 0.4s`
    },
    'a:hover': {
      color: globalTheme.linkColorHover
    },
    '@media screen and (max-width: 575.99px)': {
      h1: {
        ...adjustFontSizeTo('2.2rem'),
        lineHeight: 1.1,
      }
    }
  })
})

if (process.env.NODE_ENV !== 'production') {
  typography.injectStyles()
}

export default typography
