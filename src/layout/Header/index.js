import React from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'gatsby';

export default (props) => (
  <header {...props}>
    <Helmet>
      <title>{props.title}</title>
    </Helmet>
    <h1>
      <Link to="/">
        {props.title}
      </Link>
    </h1>
  </header>
)
