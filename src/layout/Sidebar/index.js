import React from 'react'
import { Link } from 'gatsby'
import { css } from 'emotion';

import Nav from '../../components/Nav';

export default (props) => (
  <aside className={props.className}>
    <Nav categories={props.categories} />
  </aside>
)

