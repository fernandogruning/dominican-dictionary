import React from "react"

export default (props) => (
  <footer {...props}>
    <p>&copy; 2018 &ndash; Made with <span role="img" aria-label="love">❤️</span> by <a href="https://fernandogruning.com" target="_blank" rel="noopener noreferrer">Fernando Gruning</a> &amp; <a href="http://nextrd.do" target="_blank" rel="noopener noreferrer">Marcos Zapata</a></p>
  </footer>
)
