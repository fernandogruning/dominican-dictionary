import React from 'react';
import { css } from 'emotion';

export default (props) => (
  <main className={props.className}>
    {props.children}
  </main>
)
