import { css } from 'emotion';

export default {
  layout: css({
    display: `grid`,
    gridGap: `1rem`,
    gridTemplateColumns: `1fr auto`,
    gridTemplateRows: `auto 1fr auto`,
    gridTemplateAreas:
      `"header header"
      "main sidebar"
      "footer footer"`,
    maxWidth: '60rem',
    minHeight: `100vh`,
    margin: '0 auto',
    '-webkit-font-smoothing': `antialiased`,
    '-moz-osx-font-smoothing': `grayscaled`,

    '@media screen and (max-width: 575.99px)': {
      gridGap: 0,
      gridTemplateColumns: `1fr`,
      gridTemplateRows: `auto auto auto auto`,
      gridTemplateAreas:
        `"header"
        "sidebar"
        "main"
        "footer"`,
      alignItems: `flex-start`
    }
  }),
  header: css({
    gridArea: `header`,
    flex: `1 1 100%`,
    padding: `1rem 2rem`,

    '@media screen and (max-width: 575.99px)': {
      padding: `1rem`
    }
  }),
  main: css({
    gridArea: `main`,
    padding: `0 2rem`,
    backgroundColor: `white`,
    marginBottom: `2rem`,
    'p': {
      fontSize: `1.5rem`,
      '&:last-child': {
        marginBottom: 0
      }
    },

    '@media screen and (max-width: 575.99px)': {
      padding: `0 1rem`,

      p: {
        fontSize: `1.25rem`
      }
    }
  }),
  sidebar: css({
    gridArea: `sidebar`,
    padding: `0 2rem`,

    '@media screen and (max-width: 575.99px)': {
      width: `100vw`,
      padding: `0 0.25rem`
    }
  }),
  footer: css({
    gridArea: `footer`,
    flex: `1 1 100%`,
    padding: `1rem 2rem`,
    'p': {
      marginBottom: 0
    },

    '@media screen and (max-width: 575.99px)': {
      padding: `1rem`,

      p: {
        fontSize: `0.75rem`
      }
    }
  })
}
