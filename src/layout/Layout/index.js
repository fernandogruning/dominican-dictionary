import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

import layoutStyles from './styles';

import Header from '../Header';
import Main from '../Main';
import Sidebar from '../Sidebar';
import Footer from '../Footer';

export default ({ children }) => (
  <StaticQuery
    query={query}
    render={data => <Layout data={data} children={children} />}
  />
)

const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allContentfulEntryCategory(sort: { fields: slug, order: ASC }) {
      edges {
        node {
          id
          title
          slug
          entry {
            id
          }
        }
      }
    }
  }
`

const Layout = (props) => (
  <div className={layoutStyles.layout}>
    <Header className={layoutStyles.header} title={props.data.site.siteMetadata.title} />

    <Main className={layoutStyles.main}>{props.children}</Main>

    <Sidebar categories={props.data.allContentfulEntryCategory.edges} className={layoutStyles.sidebar} />

    <Footer className={layoutStyles.footer} />
  </div>
)
