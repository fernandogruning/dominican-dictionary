import { css } from 'emotion';
import { rgba } from 'polished';

export default css({
  maxWidth: `48rem`,
  backgroundColor: `white`,
  padding: `2.5rem 2rem`,
  border: `1px solid #ececec`,
  borderRadius: `4px`,
  boxShadow: `0px 4px 6px ${rgba('#ececec', 0.4)}`,
  marginBottom: `1.25rem`,

  '@media screen and (max-width: 575.99px)': {
    padding: `1.25rem 1rem`
  }
})
