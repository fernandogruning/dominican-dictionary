import React from 'react';
import { cx } from 'emotion';

import cardStyles from './styles';

export default (props) => (
  <div className={cx(cardStyles, ...props.className)}>
    {props.children}
  </div>
)
