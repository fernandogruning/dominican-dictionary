import { css } from 'emotion';

import globalTheme from '../../utils/global-theme';

const baseStyles = css({
  display: `block`,
  width: `3rem`,
  fontSize: `1.5rem`,
  textAlign: `center`,
  textTransform: `lowercase`,
  transition: `all 0.4s`,

  '&:not(:last-child)': {
    marginBottom: `1rem`
  },

  '@media screen and (max-width: 575.99px)': {
    flex: `none`,
    heigth: `4.5rem`,
    marginBottom: `0 !important`,

    '&:not(:last-child)': {
      marginRight: `1rem`
    }
  }
})

export default (props) => {
  if (!props.noHover) {
    return css(
      baseStyles,
      {
        '&:hover': {
          fontSize: `3rem`,
          fontWeight: 600
        }
      })
  } else {
    return css(baseStyles)
  }
}

export const activeStyles = (props) => {
  if (!props.noHover) {
    return css({
      fontWeight: 600,
      fontSize: `3rem`,
      color: globalTheme.linkColorHover
    })
  } else {
    return css({
      fontWeight: 600,
      color: globalTheme.linkColorHover
    })
  }
}

export const disabledStyles = css(
  baseStyles,
  {
    color: `gray`,
    cursor: `not-allowed`
  })
