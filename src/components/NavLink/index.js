import React from 'react';
import { Link } from 'gatsby';
import { cx } from 'emotion';

import navLinkStyles, { activeStyles, disabledStyles } from './styles';

export default (props) => {
  // Exclude isNavigable from props
  const { isNavigable, ...linkProps } = props

  // check if <Link> route starts with location.pathname
  const isActive = ({ isPartiallyCurrent, isCurrent }) => (
    isPartiallyCurrent && isCurrent
      ? { className: cx(navLinkStyles(linkProps), activeStyles(linkProps)) }
      : { className: navLinkStyles(linkProps) }
  )

  if (props.isNavigable) {
    return (
      <Link {...linkProps} getProps={isActive}>{props.children}</Link>
    )
  } else {
    return (<span className={disabledStyles} title="No Entries yet">{props.children}</span>)
  }
}
