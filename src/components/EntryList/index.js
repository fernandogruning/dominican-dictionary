import React from 'react'
import { Link } from 'gatsby';

import getSlug from '../../utils/get-slug';

import entryListStyles from './styles.js'
import Card from '../Card'

export default ({ entries, categorySlug }) => (
  <Card className={entryListStyles}>
    <ul>
      {entries.map(({ title, slug, id }) => (
        <li key={id}>
          <Link to={getSlug(categorySlug, slug)}>{title}</Link>
        </li>
      ))}
    </ul>
  </Card>
)

