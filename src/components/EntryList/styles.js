import { css } from 'emotion';

import globalTheme from '../../utils/global-theme';

export default css({
  padding: `0 !important`,
  textTransform: `lowercase`,

  ul: {
    display: `flex`,
    flexDirection: `column`,
    justifyContent: `flex-start`,
    alignItems: `flex-start`,
    padding: 0,
    margin: 0,
    fontSize: `1.5rem`,
    listStyle: `none`,

    li: {
      // flex: `1 1 100%`,
      width: '100%',
      margin: 0,
      a: {
        display: `block`,
        padding: `1.5rem 2rem`,
        color: `black`,

        '&:hover': {
          color: globalTheme.linkColorHover
        },

        '@media screen and (max-width: 575.99px)': {
          padding: `1rem 1.25rem`
        }
      },

      '&:not(:last-child)': {
        a: {
          borderBottom: `1px solid #ececec`
        }
      }
    }
  }
})
