import React from 'react';
import { Link } from 'gatsby';

import entryStyles from './styles';
import Card from '../Card';

export default ({ title, descriptionHTML }) => (
  <Card className={entryStyles}>
    <h2>{title}</h2>

    <div
      dangerouslySetInnerHTML={{ __html: descriptionHTML }}
      className="entry-description"
    />
  </Card>
)
