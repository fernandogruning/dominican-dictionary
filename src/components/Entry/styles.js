import { css } from 'emotion';

export default css({
  h2: {
    textTransform: `lowercase`
  }
})
