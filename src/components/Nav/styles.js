import { css } from 'emotion';

export default css({
  display: `flex`,
  flexDirection: `column`,
  justifyContent: `flex-start`,
  height: `100%`
})
