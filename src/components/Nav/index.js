import React from 'react';

import navStyles from './styles';
import NavLink from '../NavLink';

export default (props) => {
  return (
    <nav className={navStyles}>
      <NavLink to="/all" isNavigable noHover>All</NavLink>
      {props.categories.map(({ node }) => (
        <NavLink isNavigable={node.entry !== null} key={node.id} to={node.slug}>{node.title}</NavLink>)
      )}
    </nav>
  )
}
