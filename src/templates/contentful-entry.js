import React from "react"
import { Link, graphql } from "gatsby"
import { css } from "emotion"

import getSlug from '../utils/get-slug'

import Layout from '../layout/Layout'
import Entry from "../components/Entry"

export default ({ data }) => {
  const contentfulEntry = data.contentfulEntry
  const currentEntryId = contentfulEntry.id
  const allEntries = data.allContentfulEntry.edges

  const currentEntry = allEntries.filter(({ node }) => {
    return node.id === currentEntryId
  })[0]

  let PreviousEntryLink = () => <div className="prev"></div>
  let NextEntryLink = () => <div className="next"></div>

  if (currentEntry.previous) {
    const previousEntrySlug = getSlug(currentEntry.previous.entryCategory.slug, currentEntry.previous.slug)
    PreviousEntryLink = () => (
      <div className="prev">
        <Link to={previousEntrySlug}>Anterior: <strong>{currentEntry.previous.title}</strong></Link>
      </div>
    )
  }

  if (currentEntry.next) {
    const nextEntrySlug = getSlug(currentEntry.next.entryCategory.slug, currentEntry.next.slug)
    NextEntryLink = () => (
      <div className="next">
        <Link to={nextEntrySlug}>Siguiente: <strong>{currentEntry.next.title}</strong></Link>
      </div>
    )
  }

  return (
    <Layout>
      <div className={contentfulEntryStyles}>
        <Entry
          title={contentfulEntry.title}
          descriptionHTML={contentfulEntry.description.childMarkdownRemark.html}
        />

        <div className="navigation">
          <PreviousEntryLink />
          <NextEntryLink />
        </div>
      </div>
    </Layout>
  )
}

const contentfulEntryStyles = css({
  '.navigation': {
    display: `flex`,
    justifyContent: `space-between`,

    strong: {
      textTransform: `lowercase`
    },
    '& > div': {
      flex: `1 1 50%`
    },
    '.prev': {
      textAlign: `left`
    },
    '.next': {
      textAlign: `right`
    }
  }
})

export const query = graphql`
  query($slug: String!, $categorySlug: String!) {
    contentfulEntry(slug: { eq: $slug }) {
      id
      title
      description {
        childMarkdownRemark {
          html
        }
      }
    }
    allContentfulEntry(
      sort: { fields: title, order: ASC} ,
      filter: { entryCategory: { slug: { eq: $categorySlug } } }
    ) {
      edges {
        previous {
          title
          slug
          entryCategory {
            slug
          }
        }
        node {
          id
        }
        next {
          title
          slug
          entryCategory {
            slug
          }
        }
      }
    }
  }
`
