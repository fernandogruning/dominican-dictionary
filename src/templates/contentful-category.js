import React from "react"
import { css } from 'emotion'
import { graphql } from "gatsby"

import sortEntriesByTitle from '../utils/sortEntriesByTitle';

import Layout from '../layout/Layout'
import EntryList from '../components/EntryList'

export default ({ data }) => {
  const entryCategory = data.contentfulEntryCategory

  entryCategory.entry.sort(sortEntriesByTitle)

  return (
    <Layout>
      <h1 className={h1Styles}>{entryCategory.title}</h1>

      <EntryList
        entries={entryCategory.entry.sort()}
        categorySlug={entryCategory.slug}
      />
    </Layout>
  )
}

const h1Styles = css({
  paddingLeft: `2rem`
})

export const query = graphql`
  query($slug: String!) {
    contentfulEntryCategory(slug: { eq: $slug }) {
      title
      slug
      entry {
        id
        title
        slug
      }
    }
  }
`
