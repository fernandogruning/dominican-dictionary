import React, { Component } from 'react'
import { Link, graphql } from 'gatsby';

import getRandomEntry from '../utils/get-random-entry';

import Layout from '../layout/Layout'
import Entry from '../components/Entry'

class IndexPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      randomEntry: {}
    }
  }

  componentDidMount() {
    const randomEntry = getRandomEntry(this.props.data.allContentfulEntry.edges)

    this.setState({
      randomEntry
    })
  }

  render() {
    const { randomEntry } = this.state

    return (
      <Layout>
        <h1>Welcome to DoDict!</h1>

        <h3>Here's a random entry for you:</h3>

        <Entry
          title={randomEntry.title}
          descriptionHTML={randomEntry.description}
        />

        <Link to="/all">View all Entries</Link>
      </Layout>
    )
  }
}

export const query = graphql`
  query {
    allContentfulEntry {
      edges {
        node {
          title
          description {
            childMarkdownRemark {
              html
            }
          }
          entryCategory {
            slug
          }
        }
      }
    }
  }
`

export default IndexPage
