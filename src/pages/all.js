import React from 'react'
import { Link, graphql } from 'gatsby'

import sortEntriesByTitle from '../utils/sortEntriesByTitle';

import Layout from '../layout/Layout'
import EntryList from '../components/EntryList'

export default (props) => {
  return (
    <Layout>
      <h1>All Categories</h1>
      <div>
        {props.data.allContentfulEntryCategory.edges.map(({ node }) => {
          const { id, title, slug, entry } = node

          if (entry !== null) {
            entry.sort(sortEntriesByTitle)

            return (
              <div key={id} css={{ marginBottom: `3rem`, h1: { marginBottom: `0.5rem` } }} >
                <h1>{title}</h1>

                <div>
                  <EntryList entries={entry} categorySlug={slug} />
                  <Link to={slug}>View All</Link>
                </div>
              </div>
            )
          } else {
            return (
              <div key={id} css={{ marginBottom: `3rem`, h1: { marginBottom: `0.5rem` } }} >
                <h1>{title}</h1>
              </div>
            )
          }
        })}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allContentfulEntryCategory(
      sort: { fields: title, order: ASC },
    ) {
      edges {
        node {
          id
          title
          slug
          entry {
            id
            title
            slug
          }
        }
      }
    }
  }
`
