import React, { Component } from 'react'
import { graphql } from 'gatsby';

import getRandomEntry from '../utils/get-random-entry';

import Layout from '../layout/Layout'
import Entry from '../components/Entry'

class IndexPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      randomEntry: {}
    }
  }

  componentDidMount() {
    const randomEntry = getRandomEntry(this.props.data.allContentfulEntry.edges)

    this.setState({
      randomEntry
    })
  }

  render() {
    const { randomEntry } = this.state

    return (

      <Layout>
        <h1>😳Oops! Page not found</h1>
        <p>Here's a random entry:</p>

        <Entry title={randomEntry.title} descriptionHTML={randomEntry.description} />
      </Layout>
    )
  }
}

export const query = graphql`
  query {
    allContentfulEntry {
      edges {
        node {
          title
          slug
          description {
            childMarkdownRemark {
              html
            }
          }
          entryCategory {
            slug
          }
        }
      }
    }
  }
`

export default IndexPage
